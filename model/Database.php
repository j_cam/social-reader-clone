<?php
defined('ACCESS') or die(header("Location: ../?view=home")); // Security - If the user tried to access directly return them to the ontroller


class Database {



	// Main Method
	function __construct($params) {
			
		$this->db = new mysqli(
            $params['host'], 
            $params['user'], 
            $params['password'], 
            $params['dbname']
            );

	}



    public function query($query) {
        
        $results = mysqli_query($this->db, $query);

        if($results) {
           
            return $results;
        
        } else {
           
            return false;
        
        }

        mysqli_free_result($results);
    
    }

    public function insert($query) {
        
        return mysqli_query($this->db, $query);
    }

    public function update($query) {
        
        return mysqli_query($this->db, $query);
    }    



}
	
?>