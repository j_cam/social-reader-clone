<?php
defined('ACCESS') or die(header("Location: ../?view=home")); // Security - If the user tried to access directly return them to the ontroller
ob_start();
// session_set_cookie_params(3600,"/");
session_start();
include_once("./model/Database.php");
include_once("./model/Post.php");
include_once("./model/FieldValidator.php");
include_once("./model/ContactUs.php");
require_once('./model/SwiftMailer/swift_required.php');


class Model {

	 /*
	 	Parameters for MySQLi connection
	*/
	private $dbParams = array(
           // production
           'host'=>'', 
           'user'=>'', 
           'password'=>'', 
           'dbname'=>''
		);
	 /*
	 	Parameters for SMTP
	*/	
	private $smtpHost = 'smtp.yourdomain.com';
	private $smtpPort = 465;
	private $smtpProtocol = 'ssl' ;
	private $smtpUser ='youremail@yourdomain.com';
	private $smtpPass = 'yourpassword';

	
	
	/*	**************************
		Function: setHost
		@param $url - the HTTP Host string
		@return sets the DB host string
		to the appropriate value production or dev
	*******************************	*/
	public function setHost($url) 
	{
		$devHost = 'mysql.yourdomian.com';
		$productionDomain = 'yourdomain.com';
		$productionHost = 'yourdomain.yourhostmysql.com';

		if(strpos($url, $productionDomain)) 
		{ 
			// production
			$this->dbParams['host'] = $productionHost;
		}
		else 
		{
			// dev
			$this->dbParams['host'] = $devHost; 
		}
	}

	/*	**************************
		SECURITY
		Function: sanitizeData
		@param $inputData - the string to be sanatized
		@return the sanatized string
	*******************************	*/
	public function sanitizeData($inputData) 
	{
		return htmlentities(stripslashes($inputData), ENT_QUOTES);
	}


	/*	**********************************************************
		Function: getPostList
		@param $aryOptions - an array of options empty or othrwise 
		@return a MySQLi query object
	*********************************************************** */
	public function getPostList($aryOptions)
	{
	
		$db = new Database($this->dbParams);
		
		if(!empty($aryOptions))
		{
			// QUERY: all posts
			$qry = "SELECT * FROM databasename.TBL_POSTS WHERE POST_CATEGORY = '$aryOptions[category]'";
			
		}
		else
		{
			// QUERY: all posts sort by effectiveVote and date
			$qry = "SELECT * FROM databasename.TBL_POSTS ORDER BY (POST_UPVOTE - POST_DOWNVOTE) DESC, POST_DATETIME DESC";		
		} 
		// return the results to the caller
		return $db->query($qry);
	}


	
	/* **************************
		VOTING
	**************************** */

	 /*
	 	Returns an array whose first element is 
	 	votes_up and the second one is votes_down
	 	for a given POST_ID
	*/
	public function getAllVotes($id) 
	{

 		$votes = array();
 	
 		$qry = "SELECT * FROM databasename.TBL_POSTS WHERE POST_ID = $id";
 		$db = new Database($this->dbParams);
 		$r = $db->query($qry);

	 	if(mysqli_num_rows($r)==1)//id found in the table
	  	{
		  	$row = mysqli_fetch_assoc($r);
		  	$votes[0] = $row['POST_UPVOTE'];
		  	$votes[1] = $row['POST_DOWNVOTE'];
	  	}
 		
 		return $votes;
 	}


	/*	*********************************
		Function: getEffectiveVotes
		@param $id- the id of the post
		@return the calculated vote total 
	************************************ */
 	public function getEffectiveVotes($id) 
 	{
 		$votes = $this->getAllVotes($id);  
 		$effectiveVote = $votes[0] - $votes[1];  
 		return $effectiveVote;  
 	}


	/*	*********************************
		Function: voteUp
		@param $id - the id of the post
		@return the calculated vote total 
	************************************ */
	public function voteUp($id) 
	{	
		//get the current votes
		$cur_votes = $this->getAllVotes($id);
		$votes_up = $cur_votes[0]+1;
		//ok, now update the votes
		$qry = "UPDATE databasename.TBL_POSTS SET POST_UPVOTE = $votes_up WHERE POST_ID = $id";
		echo $this->getUpdatedVotes($id, $qry);
	}


	//voting down
	public function voteDown($id) 
	{
		//get the current votes
		$cur_votes = $this->getAllVotes($id);
		$votes_down = $cur_votes[1]+1;
		$qry = "UPDATE databasename.TBL_POSTS SET POST_DOWNVOTE = $votes_down WHERE POST_ID = $id";
		echo $this->getUpdatedVotes($id, $qry); 
	}


	public function getUpdatedVotes($id, $qry) 
	{
		$db = new Database($this->dbParams);
		$result = $db->update($qry);
		if($result) //voting done
		{
		 $effectiveVote = $this->getEffectiveVotes($id);
		 return "score: " . $effectiveVote;
		}
		elseif(!$result) //voting failed
		{
			return "Vote Failed!";
		}
	}

	/* **************************
		CONTACT US
	**************************** */
	
	public function sendMessage($postVars) 
	{

		// Create the ContactUs Object with the given POST Variables
		$contactUs = new ContactUs($postVars);

		// Validate the object
		$validPost = $contactUs->validate();

		/*	**************************
			SECURITY
			Check for CAPTCHA error
			**************************
		*/
		include_once('./view/securimage/securimage.php');
		$securimage = new Securimage();
		
		$captchaError = false;

		if ($securimage->check($postVars['captcha_code']) == false) 
		{
			// the code was incorrect
			// you should handle the error so that the form processor doesn't continue
			// or you can use the following code if there is no validation or you do not know how
			$captchaError = 'true';
		}

		// Did it validate & was the Captcha code correct?
		if($validPost == "success" && $captchaError != true ) 
		{

			// TRY SENDING
			// Create the Transport
			$transport = Swift_SmtpTransport::newInstance($this->smtpHost, $this->smtpPort, $this->smtpProtocol);
			$transport->setUsername($this->smtpUser);
			$transport->setPassword($this->smtpPass);
			
			// Create the Mailer using th created Transport
			$swiftMailer = Swift_Mailer::newInstance($transport);
			// Create a message
			$swiftMessage = Swift_Message::newInstance();
			// Set user input email
			$swiftMessage->setTo($contactUs->getSenderEmail());
			// check for CC
			
			if($contactUs->getCarbonCopy()) 
			{
				$swiftMessage->setCc($contactUs->getSenderEmail());
			}
			
			// set from email/Admin email
			$swiftMessage->setFrom(array($contactUs->getAdminEmail() => 'yourdomain.com'));
			// set email subject
			$swiftMessage->setSubject($contactUs->getSubject());
			// set the message and then set the body of the email;
			$contactUs->setMessage();
			$swiftMessage->setBody($contactUs->getMessage(), "text/html");
			// Send the message
			$result = $swiftMailer->send($swiftMessage);
			
			// check the result
			if($result) 
			{
				return $validPost;
			}
			else
			{
				$error = '&send-mail="The Internet is not playing nice. Please try again."';
				return 'error' . $error;
			}
		}
		// Did not validate
		else 
		{
			//if captcha only			
			if($captchaError = true && $validPost == "success" ) 
			{
				return 'error' . '&captcha-error="The security code entered was incorrect"';
				exit();
			}
			// if error only
			if($validPost !== "success" && $captcha_error = false) 
			{
				return 'error' .  $validPost;
				exit();
			}
			// capthca and error
			else 
			{
				$validPost .= '&captcha-error="The security code entered was incorrect"';
				return $validPost;
				exit();
			} 
		}
	} // sendMessage
	

	/* **************************
		Submit Link/Post
	**************************** */
	public function submitPost($postVars)
	{
		// Create the ContactUs Object with the given POST Variables
		$submitPost = new Post($postVars);
		$submitPost->setSource();

		// Validate the object
		$validPost = $submitPost->validate();

		// Check for CAPTCHA error
		include_once('./view/securimage/securimage.php');
		$securimage = new Securimage();
		
		$captchaError = false;

		if ($securimage->check($postVars['captcha_code']) == false) 
		{
			// the code was incorrect
			// you should handle the error so that the form processor doesn't continue
			// or you can use the following code if there is no validation or you do not know how
			$captchaError = 'true';
		}

		// Did it validate & was the Captcha code correct?
		if($validPost == "success" && $captchaError != true ) 
		{

			$db = new Database($this->dbParams);
			// QUERY: insert post
			
			/* 
			____________________________________________ 
				Security -
				Added addslashes(str) to user input post variables 
				dont add slashes to source because it needs to be
				parsed as a URL
			____________________________________________
			*/
			$link = strip_tags($_POST['link']);
			$title = addslashes(strip_tags($_POST['title']));
			$source = $submitPost->getSource();
			$category = addslashes(strip_tags($_POST['category']));
			$qry = "INSERT INTO 
				databasename.TBL_POSTS (POST_LINK, POST_TITLE, POST_SOURCE, POST_CATEGORY
				)VALUES(
					'$link',
					'$title',
					'$source',
					'$category'
				)";
			// return the results to the caller

			$result = $db->insert($qry);
			
			// check the result
			if($result) {
				return $result;
			}
			else
			{
				$error = '&send-mail="The Internet is not playing nice. Please try again.:)"';
				return 'error' . $error;
			}
		}
		// Did not validate
		else 
		{
			//if captcha only			
			if($captchaError = true && $validPost == "success" ) 
			{
				return 'error' . '&captcha-error="The security code entered was incorrect"';
				exit();
			}
			// if error only
			if($validPost !== "success" && $captcha_error = false) 
			{
				return 'error' .  $validPost;
				exit();
			}
			// capthca and error
			else 
			{
				$validPost .= '&captcha-error="The security code entered was incorrect"';
				return 'error' . $validPost;
				exit();
			} 
		}			
	} // submitPost

}
?>