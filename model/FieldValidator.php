<?php
defined('ACCESS') or die(header("Location: ../?view=home")); // Security - If the user tried to access directly return them to the ontroller


class FieldValidator {

	private $fields;


	// Set of fields
    /*
        Creates an array of necessary form 
        field names to validate against
    */
	public function setFields($fields) {

        $this->fields = $fields;

	}


    // Set of fields
    public function validateFields($postVars) {
       
        foreach ($postVars as $value) {
            if(!empty($value)) {
                $valid = true;
            }
            else {
                $valid = false;
                break;
            } 
        }
            return $valid;

    }


    // URL Check
    public function validate($field, $type) {

        if(empty($field)) {
            return false;
        }


        elseif($type == 'url') {
           return filter_var($field, FILTER_VALIDATE_URL);
        }

        elseif($type == 'email') {
            return filter_var($field, FILTER_VALIDATE_EMAIL);
        }




    }


}
?>