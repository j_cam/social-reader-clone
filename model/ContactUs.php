<?php
defined('ACCESS') or die(header("Location: ../?view=home")); // Security - If the user tried to access directly return them to the ontroller


class ContactUs {

	// Instance Variables
	public $senderEmail;
	public $senderFname;
	public $senderLname;	
	public $adminEmail;
	public $subject;
	public $message;
	public $carbonCopy;

	// Main Method
	function __construct($postVars) {

		$this->senderEmail = $postVars['email'];
		$this->senderFname = $postVars['fname'];
		$this->senderLname = $postVars['lname'];
		$this->adminEmail = 'jcampbelldevdummy@gmail.com';
		$this->subject = $postVars['subject'];
		$this->message  = $postVars['message'];
		
		if(isset($postVars['carbon-copy'])) {

			$this->carbonCopy = true;
		}
		else {
			$this->carbonCopy = false;
		}
	}

	/* **************************
		GETTERS
	**************************** */
	public function getSenderEmail(){
		return $this->senderEmail;
	}

	public function getAdminEmail(){
		return $this->adminEmail;
	}

	public function getMessage(){
		return $this->message;
	}

	public function getSubject(){
		return $this->subject;
	}


	public function getCarbonCopy(){
		return $this->carbonCopy;
	}	


	/* **************************
		SETTERS
	**************************** */
	public function setSenderEmail($var){
		$this->senderEmail = $var;
	}

	public function setAdminEmail($var){
		$this->adminEmail = $var;
	}

	public function setSubject($var){
		$this->subject = $var;
	}

	public function setCarbonCopy($var){

			$this->carbonCopy = $var;

	}

	public function setMessage($var){

	$emailBody = "
	<html><head><title>Daily Hot Links Contact Message</title></head><body>
		<h1>Daily Hot Links Contact Message</h1>
		<table style='border:solid 1px #dddddd;background: white; text-align:left;' cellpadding='10' border='1' cellspacing='0'>
		<tr>
		  <th style='background:#cccccc;' colspan='2'><h2>Details</h2></th>
		</tr><tr>
		  <td><b>Regarding</b></td><td><p>". $this->subject . "</p></td>
		</tr><tr>
		  <td><b>From</b></td><td><p>" . $this->senderFname. "&nbsp;" . $this->senderLname . "</p></td>
		</tr><tr>
		  <td><b>Email</b></td><td><p><a href='mailto:'" . $this->senderEmail . ">" . $this->senderEmail . "</a></p></td>
		</tr><tr>
		  <td colspan='2'><b>Message Body</b><br><p>" . $this->message . "</p></b></td>
		</tr></table></body></html>";
		
		$this->message = $emailBody;

	}




	/* ***********************************************************
		VALIDATION
		@return string 
		return values: 
		1. success (for a valid set of variables)
		2. formatted get string with errors found as a $_GET string
	*********************************************************** */
	public function validate() {
		
		// Check that necessary properties are set
		/*if (empty($this->senderEmail) | empty($this->subject)) 
		{
			echo 'error';
		}*/
		$error = '';
		// Check that necessary properties are set
		if (empty($this->senderFname)) 
		{
			$error .= '&fname-error="First name is required"';
		}
		if (empty($this->senderLname)) 
		{
			$error .= '&lname-error="Last name is required"';
		}		
		if (empty($this->senderEmail)) 
		{
			$error .= '&email-error="Email is required"';
		}
		if (empty($this->subject)) 
		{
			$error .= '&subject-error="Reason is required"';
		}
		if (trim($this->message) == "") 
		{
			$error .= '&message-error="Message is required"';
		}
		if($error !== ''){
			return 'error' . $error;
		}		
		else
		{

			return 'success';

		}
	}

}

?>