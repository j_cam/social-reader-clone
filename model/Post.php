<?php
defined('ACCESS') or die(header("Location: ../?view=home")); // Security - If the user tried to access directly return them to the ontroller
class Post {

	public $link;
	public $title;
	public $source;
	public $upvote;
	public $downvote;
	public $category;
	
	public function __construct($postVars)  
    {  
        $this->link = $postVars['link'];;
	    $this->title = $postVars['title'];;
	    $this->source = '';
	    $this->upvote = 1;
	    $this->downvote = 0;
	    $this->category = $postVars['category'];;
    }


    /* **************************
        GETTERS
    **************************** */
    public function getLink() {
        return $this->link; 
    }
    public function getSource() {
        return $this->source;
    }  
    public function getUpvote() {
        return $this->upvote; 
    }
    public function getCategory() {
        return $this->category; 
    }
    /* **************************
        SETTERS
    **************************** */   
    public function setLink($value) {
    	$this->link = $value; 
    }
    public function setTitle($value) {
    	$this->title = $value; 
    }
    public function setSource() {    
        $url = $this->link;
        $urlArray = parse_url($url);
        $this->source = $urlArray['host'];
    }
    public function setDownvote($value) {
    	$this->downvote = $value; 

    }

    /* ***********************************************************
        VALIDATION
        @return string 
        return values: 
        1. success (for a valid set of variables)
        2. formatted get string with errors found as a $_GET string
    *********************************************************** */
    public function validate() {

        // Check that necessary properties are set
        $error = '';
        // Check that necessary properties are set
        if (empty($this->title)) 
        {
            $error .= '&title-error="A title is required"';
        }
        if (empty($this->link)) 
        {
            $error .= '&link-error="A URL is required"';
        }       
        if (empty($this->category)) 
        {
            $error .= '&category-error="A category is required"';
        }
        if($error !== ''){
            return $error;
        }     
        else
        {

            return 'success';

        }
    }



}

?>