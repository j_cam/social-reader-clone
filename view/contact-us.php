<?php
  require_once('common/header.php');
  require_once('common/navigation.php');
 
  // Vaules for error validaton 
  $formName ="ContactUs";

    // Set select value to false
    $s = false;
    //unless it's set
    if(isset($_SESSION[$formName]['subject'])){
      // get the selected value
      $s = true;
      $sVal = $_SESSION[$formName]['subject'];
    }

?>
<!-- CONTENT -->

<div class="row">
	<div class="large-12 columns">
		<h2>Contact Us</h2>
		<p>Send us a line to let us know how we're doing.</p>
		<hr />
	</div>
</div>
  <?php if (isset($_GET['success']) | isset($_GET['error'])): ?>
  <div class="row entry">
    <div class="large-12 columns">  
      
      <?php if (isset($_GET['success'])): ?>
      <div data-alert class="alert-box success">
        Thank you for contacting us. If necessary, we will respond to you within 48hrs.
        <a href="#" class="close">&times;</a>
      </div>
      <?php endif; ?>
      
      <?php if (isset($_GET['error'])): ?>    
      
      <div data-alert class="alert-box alert error">
        Please correct the fields in red
        <a href="#" class="close">&times;</a>
      </div>
      
      <?php  

        // Additional Errors
        $fname_error = isset($_GET['fname-error']);
        $lname_error = isset($_GET['lname-error']);
        $email_error = isset($_GET['email-error']);
        $subject_error = isset($_GET['subject-error']);
        $message_error = isset($_GET['message-error']);
        $captcha_error = isset($_GET['captcha-error']); 

        endif; 
      ?>    

    </div>
  </div>
  <?php endif; ?>
<div class="row">
  <form name="<?php echo $formName ?>" class="large-12 columns custom" action="<?php echo htmlentities($_SERVER['PHP_SELF']).'?action=contact-us' ?>" method="post">
    <fieldset>
      <legend>Message Information</legend>    

      <div class="row">
        <div class="large-6 columns <?php if($fname_error) echo 'error'?>">
          <label>First Name</label>
          <input type="text" name="fname" placeholder="First Name" 
          value="<?php if(isset($_SESSION[$formName]['fname']) && !isset($_GET['success'])){echo $_SESSION[$formName]['fname'];} ?>" >
        </div>

        <div class="large-6 columns <?php if($lname_error) echo 'error'?>">
          <label>Last Name</label>
          <input type="text" name="lname" placeholder="Last Name" value="<?php if(isset($_SESSION[$formName]['lname']) && !isset($_GET['success'])){echo $_SESSION[$formName]['lname'];} ?>" >
        </div>
      </div>        

      <div class="row">
        <div class="large-6 columns <?php if($email_error) echo 'error'?>">
          <label>Email Address</label>
          <input type="email" name="email" placeholder="Email Address" 
          value="<?php if(isset($_SESSION[$formName]['email'])&& !isset($_GET['success'])){echo $_SESSION[$formName]['email']; } ?>" >
        </div>
        <div class="large-6 columns <?php if($subject_error) echo 'error'?>">
        <label>Reason For Contact</label>
        <!-- CATEGORY -->
        <select name="subject" placeholder="large-4.columns" >
          <option value="">Select a Reason</option>


          <option value="Feature Request" <?php if($s && $sVal == "Feature Request" && !isset($_GET['success'])){echo 'selected';}?> >Feature Request</option>
          <option value="Bug Report" <?php if($s && $sVal == "Bug Report" && !isset($_GET['success'])){echo 'selected';}?>>Bug Report</option>
          <option value="Business Inquiry" <?php if($s && $sVal == "Business Inquiry" && !isset($_GET['success'])){echo 'selected';}?>>Business Inquiry</option>
          <option value="General Inquiry" <?php if($s && $sVal == "General Inquiry" && !isset($_GET['success'])){echo 'selected';}?>>General Inquiry</option>
        </select>
        </div>
      </div>      

      <div class="row">
        <div class="large-12 columns <?php if($message_error) echo 'error'?>">
          <label>Message</label>
          <textarea name="message" placeholder="Enter your message..." ><?php if(isset($_SESSION[$formName]['message']) && !isset($_GET['success'])){echo $_SESSION[$formName]['message'];} ?></textarea>
        </div>
      </div>
      <div class="row">
        <div class="large-6 columns <?php if($captcha_error) echo 'error'; ?>">
          <label>Security Code<span></label>
          <img id="captcha" src="./view/securimage/securimage_show.php" alt="CAPTCHA Image" />
          <br>
          <input type="text" name="captcha_code" size="10" maxlength="6" placeholder="enter security code" />
          <a class="button tiny secondary" href="#" onclick="document.getElementById('captcha').src = './view/securimage/securimage_show.php?' + Math.random(); return false">New Security Code Image</a>      
        </div>
      </div>
      <hr>
      <div class="row">
      <div class="large-6 columns">
        <label for="carbon-copy">
          <input name="carbon-copy" type="checkbox" id="carbon-copy">
          Send Me a copy for my records
        </label>
      </div>
      <input type="hidden" name="formName" value="<?php echo $formName ?>">
    </fieldset>
    <input name='submit' type="submit" class="large button" value="submit">
  </form>
</div>

<!-- END CONTENT -->
<?php require_once('common/footer.php'); ?> 