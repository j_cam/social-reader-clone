<?php require_once('common/header.php'); ?>
<?php require_once('common/navigation.php'); ?>
<!-- CONTENT -->
<div class="row">
	<div class="large-12 columns">
		<h2>Welcome to Social Reader</h2>
		<p>Version 0.0.1</p>
		<hr />
	</div>
</div>
	
<?php 
	// Show success message if they just submitted a post
	if (isset($_GET['success'])): 
?>
<div class="row entry">
	<div class="large-12 columns">	
		<div data-alert class="alert-box success">
			Thank you for your submission!
			<a href="#" class="close">&times;</a>
		</div>
	</div>
</div>
<?php endif; ?>

<?php 
	while ($row = mysqli_fetch_array($posts))  {
	$effective_vote = $row['POST_UPVOTE'] - $row['POST_DOWNVOTE'];
?>					
<div class="row">
	<div class="large-12 columns panel radius">
			<div class="large-8 columns">
				<h4 class="link">
					<a href="<?php echo $row['POST_LINK']; ?>">
						<?php echo $row['POST_TITLE']; ?>
					</a>
				</h4>
			
				
				<ul class="inline-list">
					<li class="keystroke"><?php echo $row['POST_CATEGORY']; ?></li>
					<li class="keystroke"><?php echo $row['POST_SOURCE']; ?></li>
					<li class="keystroke"><?php echo date("g:i a F j, Y ", strtotime($row['POST_DATETIME'])) ?></li>
				</ul>
			</div>
			<div class="large-4 columns panel callout radius ">
					<h5 class='votes_count' id='votes_count<?php echo $row['POST_ID']; ?>'>
						score: <?php echo $effective_vote; ?>
					</h5>
	
					<h6 class='vote_buttons' id='vote_buttons<?php echo $row['POST_ID']; ?>'>Vote:   
			  			<a href='javascript:;' class='vote_up label success' id='<?php echo $row['POST_ID']; ?>'>+1</a> |   
			  			<a href='javascript:;' class='vote_down alert label' id='<?php echo $row['POST_ID']; ?>'>-1</a>  
			  		</h6>				
			</div>
	  	</div><!-- / .panel -->
</div><!-- end row.entry -->

<?php } ?>

<!-- END CONTENT -->
<?php require_once('common/footer.php'); ?>

