<?php require_once('common/header.php'); ?>
<?php require_once('common/navigation.php'); ?>
<!-- CONTENT -->
<div class="row">
	<div class="large-12 columns">
		<h2>How It Works</h2>
		<p>Submit + Peruse + Vote = Have Fun </p>
		<hr />
	</div>
</div>
<div class="row">
	<div class="large-12 columns">
	<h3>What is it?</h3>
	<p>
		Social Reader is simply a place where you can anonymously share links of interest that you find on the web with other people on the web. And let's face it, you have nothing better to do while you're at work being a laggard and goofing off on the commpany dime. 
	</p>

	<h3>How does the voting/scoring work?</h3>
	<p>
		We use a sophiticated algorithm that would make the electoral college blush. We give your post one vote when you submit it, because we figure you already like it. Then we let the community vote it up or down (as many times as they want). Then we take the total number of "+1" and subtract the
		total number of "-1" (i.e. (+3) - (-1) = (2))! <em>It's crazy, we know</em>. 
	<p>
	<h3>What makes it any different than Reddit or SlashDot?</h3>
	<p>
		Let's face it, those sites are full of lame opinions and a heavy helping of snark, that just gets annoying. Besides, those sites are locked down like Akatraz. Social Reader is cobbeled together with duct tape. You should buld a bot to post and vote for you, so you can brag to your friends about what an awesome hacker you are.  
	</p>

	</div>
</div>

<!-- END CONTENT -->
<?php require_once('common/footer.php'); ?>