$(function(){

  // load foundation
  $(document).foundation(

    {
      index : 0,
      stickyClass : 'sticky',
      custom_back_text: true, // Set this to false and it will pull the top level link name as the back text
      back_text: 'Back', // Define what you want your custom back text to be if custom_back_text: true
      init : false
    }

  );

  // vote post up
  $("a.vote_up").click(function(){  
    
    //get the id  
    the_id = $(this).attr('id');  

    // show the spinner  
    $(this).parent().html("<img src='./view/assets/images/spinner.gif'/>");  

    //fadeout the vote-count   
    $("#votes_count"+the_id).fadeOut("fast");  

    //the main ajax request  
    $.ajax({  
      type: "POST",  
      data: "action=vote_up&id="+$(this).attr("id"),  
      url: "./index.php",  
      success: function(msg)  
      { 
        $("#votes_count"+the_id).html(msg);  
        // fadein the vote count  
        $("#votes_count"+the_id).fadeIn();  
        // remove the spinner  
        $("#vote_buttons"+the_id).remove();  
      }
    });
  });


  $("a.vote_down").click(function(){
    //get the id
    the_id = $(this).attr('id');

    // show the spinner
    $(this).parent().html("<img src='./view/assets/images/spinner.gif'/>");

    //the main ajax request
    $.ajax({
      type: "POST",
      data: "action=vote_down&id="+$(this).attr("id"),
      url: "./index.php",
      success: function(msg) 
      {
        $("#votes_count"+the_id).html(msg);  
        // fadein the vote count  
        $("#votes_count"+the_id).fadeIn();  
        // remove the spinner  
        $("#vote_buttons"+the_id).remove();
      }
    });
  });

  // Custom Foundation Select focus error fix
  // https://github.com/zurb/foundation/issues/1741
  $("form.custom select").on('invalid', function(e){
    alert($(this).attr('name')+' is required');
    e.preventDefault();
    return false;
  })


});

