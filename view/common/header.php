<?php defined('ACCESS') or die(header("Location: ../?view=home")); // Security - If the user tried to access directly return them to the ontroller ?>
<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

<head>
	<meta charset="utf-8" />
  <meta name="viewport" content="width=device-width" />
  <title></title>

  <link rel="stylesheet" href="./view/assets/css/normalize.css" />
  
  <link rel="stylesheet" href="./view/assets/css/app.css" />
  <link rel="stylesheet" href="./view/assets/css/style.css" />
  

  <script src="./view/assets/scripts/vendor/custom.modernizr.js"></script>

</head>
<body>