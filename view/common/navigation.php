<div class="contain-to-grid sticky">
  <nav class="top-bar">
    <ul class="title-area">
      <!-- Title Area -->
      <li class="name">
        <h1><a href="./">Social Reader</a></h1>
      </li>
      <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
      <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
    </ul>

    <section class="top-bar-section">
      <!-- Left Nav Section -->
      <ul class="left">
        <li class="divider"></li>
        <li class="has-dropdown"><a href="./">View All</a>
          <ul class="dropdown">
                <li><label>Categories</label></li>
                <li><a href="./?filter=technology">Technology</a></li>
                <li><a href="./?filter=humor">Humor</a></li>
                <li><a href="./?filter=news">News</a></li>
              </ul>
        </li>
        <li class="divider"></li>
        <li><a href="./?view=submit">Submit A Link</a></li>
        <li class="divider"></li>
        <li><a href="./?view=how-it-works">How It Works</a></li>
        <li><a href="./?view=contact-us">Contact Us</a></li>
        <li class="divider"></li>
      </ul>
      <ul class="right">
        <li>
          <a class="button" href="https://bitbucket.org/j_cam/social-reader-clone/src">Project on Bitbucket</a>
        </li>
      </ul>
    </section>
  </nav>
</div>