  <script>
  document.write('<script src=' +
  ('__proto__' in {} ? './view/assets/scripts/vendor/zepto' : 'assets/scripts/vendor/jquery') +
  '.js><\/script>')
  </script>
  
  <script src="./view/assets/scripts/foundation/foundation.js"></script>
	
	<script src="./view/assets/scripts/foundation/foundation.alerts.js"></script>
	
	<script src="./view/assets/scripts/foundation/foundation.clearing.js"></script>
	
	<script src="./view/assets/scripts/foundation/foundation.cookie.js"></script>
	
	<script src="./view/assets/scripts/foundation/foundation.dropdown.js"></script>
	
	<script src="./view/assets/scripts/foundation/foundation.forms.js"></script>
	
	<script src="./view/assets/scripts/foundation/foundation.joyride.js"></script>
	
	<script src="./view/assets/scripts/foundation/foundation.magellan.js"></script>
	
	<script src="./view/assets/scripts/foundation/foundation.orbit.js"></script>
	
	<script src="./view/assets/scripts/foundation/foundation.placeholder.js"></script>
	
	<script src="./view/assets/scripts/foundation/foundation.reveal.js"></script>
	
	<script src="./view/assets/scripts/foundation/foundation.section.js"></script>
	
	<script src="./view/assets/scripts/foundation/foundation.tooltips.js"></script>
	
	<script src="./view/assets/scripts/foundation/foundation.topbar.js"></script>
	<script src="./view/assets/scripts/main.js"></script>
	
</body>
</html>