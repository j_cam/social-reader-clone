<?php
  require_once('common/header.php');
  require_once('common/navigation.php');
  

 
  $formName ="SubmitPost";



    
    // Set select value to false
    $s = false;
    //unless it's set
    if(isset($_SESSION[$formName]['category'])){
      // get the selected value
      $s = true;
      $sVal = $_SESSION[$formName]['category'];
    }

?>
<!-- CONTENT -->

<div class="row">
	<div class="large-12 columns">
		<h2>Submit a Link to Social Reader</h2>
		<p>Did you find something awesome on the interwebs?  Be awesome, and share it Share it with the DailyHotLinks community.</p>
		<hr />
	</div>
</div>
<div class="row">
  <form name="<?php echo $formName ?>" class="large-12 columns custom" action="<?php echo htmlentities($_SERVER['PHP_SELF']).'?action=submitPost' ?>" method="post">
    <fieldset>
      <legend>Link Information</legend>
        
        <?php if (isset($_GET['error'])): ?>
          
          <div data-alert class="alert-box alert">
            Please correct the fields in red
            <a href="#" class="close">&times;</a>
          </div>

        <?php endif;?>
            
        <?php  
          // Additional Errors
          $title_error = isset($_GET['title-error']);
          $url_error = isset($_GET['link-error']);
          $category_error = isset($_GET['category-error']);
          $captcha_error = isset($_GET['captcha-error']); 
        ?>            

      <div class="row">
        <div class="large-12 columns <?php if($title_error) echo 'error'?>">
          <label>Title</label>
          <input type="text" name="title" placeholder="Title"  value="<?php if(isset($_SESSION[$formName]['title'])&& !isset($_GET['success'])){echo $_SESSION[$formName]['title']; } ?>">
        </div>
      </div>

      <div class="row">
        <div class="large-6 columns <?php if($url_error) echo 'error'?>">
          <label>URL</label>
          <input type="text" name="link" placeholder="http://domain.com" value="<?php if(isset($_SESSION[$formName]['link'])&& !isset($_GET['success'])){echo $_SESSION[$formName]['link']; } ?>">
        </div>
        


        <div class="large-6 columns <?php if($category_error) echo 'error'?>">
  		  <label>Category</label>
    		<!-- CATEGORY -->
    		<select name="category" placeholder="large-4.columns">
    			<option value="">Select a Category</option>
    			<option value="Humor" <?php if($s && $sVal == "Humor" && !isset($_GET['success'])){echo 'selected';}?>>Humor</option>
    			<option value="Technology" <?php if($s && $sVal == "Bug Report" && !isset($_GET['success'])){echo 'selected';}?>>Technology</option>
    			<option value="News" <?php if($s && $sVal == "News" && !isset($_GET['success'])){echo 'selected';}?> >News</option>
          <option value="Other" <?php if($s && $sVal == "Other" && !isset($_GET['success'])){echo 'selected';}?> >Other</option>
    		</select>
        </div>
      </div>



      <div class="row">
        <div class="large-6 columns <?php if($captcha_error) echo 'error'; ?>">
        <label>Security Code<span></label>
        <img id="captcha" src="./view/securimage/securimage_show.php" alt="CAPTCHA Image" />
        <br>
        <input type="text" name="captcha_code" size="10" maxlength="6" placeholder="enter security code" />
        <a class="button tiny secondary" href="#" onclick="document.getElementById('captcha').src = './view/securimage/securimage_show.php?' + Math.random(); return false">New Security Code Image</a>
        <input type="hidden" name="formName" value="<?php echo $formName ?>">
    </fieldset>
    <input name="submit" type="submit" class="large button" value="submit">
  </form>
</div>

<!-- END CONTENT -->
<?php require_once('common/footer.php'); ?> 