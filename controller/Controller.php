<?php
defined('ACCESS') or die(header("Location: ../?view=home")); // Security - If the user tried to access directly return them to the ontroller
include_once("./model/Model.php");

class Controller {
	public $model;
	
	public function __construct()  
    {  
        $this->model = new Model();
        $this->model->setHost($_SERVER['HTTP_HOST']);

    } 

	
	public function invoke()
	{

		// Set session vars to repopulate form on error
		/* 
			Function: setSessionVars
			@param 1: an HTTP POST ARRAY
			@param 2: a string identifying the post array
			@return an updated SESSION array with values
			identified by the param 2 identifier 
		*/
		function setSessionVars($post, $formName) 
		{
		    foreach($post as $fieldname => $fieldvalue) 
		    {
		        /* SECURITY - added strip tags to keep 
		        session vars from containing malicious xss code */
		        $_SESSION[$formName][$fieldname] = strip_tags($fieldvalue);
	        }   
	    }


		// Check for ajax calls
		function isAjax()
		{
		    return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
		            $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest');
		}



		if(isAjax())
		{
			
			if(isset($_POST['action']))
			{
				$action = $_POST['action'];
				
				if($action == 'vote_up')
				{
					$this->model->voteUp($_POST['id']);
				}
				
				if($action == 'vote_down')
				{
					$this->model->voteDown($_POST['id']);
				}				
			}
		}

		// Requested View
		elseif (isset($_GET['view']))
		{
			// if it's the home page get the posts
			if($_GET['view'] == 'index' || 'home')
			{
				$posts = $this->model->getPostList(array());
			}
			
			
			$viewFile = './view/'. $_GET['view'] .'.php';
			
			if(file_exists($viewFile))
			{
				// show the requested view if it exsists
				include_once($viewFile);
			}
			else
			{
				// otherwise show home page
				include_once('./view/index.php');
			}
		}
		
		// Is there and action attached?
		elseif (isset($_GET['action']))
		{
			$action = $_GET['action'];

			/* **************************
				CONTACT US
			**************************** */
			if($action == 'contact-us')
			{
				$submit = $_POST;
				$formName = $submit['formName'];

				// If form is submitted, call the function and save all data into the array $_SESSION['form'] 
				if($submit){setSessionVars($submit, $formName);} 

					$success = $this->model->sendMessage($submit);

					// The message has been sent
					if($success == 'success')
					{
						header("Location: ./?view=contact-us&success=1");
						exit();
					}
					// Houston, we have a problem
					else 
					{
						header("Location: ./?view=contact-us&". $success);
						exit();
					}
			}


			/* **************************
				SUBMIT POST
			**************************** */
			if($action == 'submitPost')
			{
	

				$submit = $_POST;
				$formName = $submit['formName'];

				// If form is submitted, call the function and save all data into the array $_SESSION['form'] 
				if($submit){setSessionVars($submit, $formName);} 
					
				$success = $this->model->submitPost($submit);

				// The message has been sent
				if($success == 'success')
				{
					header("Location: ./?&success=1");
					exit();
				}
				// Houston, we have a problem
				else 
				{
					header("Location: ./?view=submit&". $success);
					exit();
				}

			}
		}
		/* **************************
			FILTERED VIEW
		**************************** */
		else
		{
			if(isset($_GET['filter']))
			{
				
				/* 
				____________________________________________ 
					Security - sanitize function in model to 
					sanitize $_GET data
				____________________________________________
				*/
				// Show filtered posts by specified category
				$filter = $this->model->sanitizeData($_GET['filter']);
				$posts = $this->model->getPostList(array('category'=>$filter));			
			}
			else
			{
				// Show all post categories
				$posts = $this->model->getPostList(array());
				
			}
			include_once('./view/index.php');
		}		

	}
}

?>